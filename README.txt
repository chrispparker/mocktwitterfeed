----------------------
Command Line Arguments
----------------------

Example:  java -jar MockTwitterConsoleApp.jar <path_to_user_file> <path_to_tweet_file>

----------------------
Building
----------------------

To build the code, run a maven clean install. This will generate a JAR file
which can be run on the command line. Unit tests will also be run as part
of the build process. The unit tests test the following:

- The processLine routines that process the input files handle errors as expected
- Test for non-ASCII-7-bit strings
- Tests for correct handling of tweets longer than 140 characters or empty tweets
- Tests that any user in the tweet file is also in the user file

----------------------
Design Decisions
----------------------

In order to solve the twitter feed problem, I decided to make use of a
Spring Boot application. Spring boot allows a developer to quickly get started
with a basic Spring application without having to manage complex sets of
Spring dependencies.

Users:

I solved the problem by parsing the user file and ensuring that the input file
is in an ASCII 7-bit format. If it is not, the application will throw an exception
and quit. Each *new* user (be this a primary user or following user) is
added to a sorted map keyed on username. This takes care of the sorting as well
as makes the lookup of users for linking to tweets easier. Each user object
contains a list of tweet ID's used to indicate which tweet(s) belong to that user
or anyone he / she follows.

Tweets:

The tweet file, similarly to the user file (and hence in an abstract base class)
is parsed and validated to ensure it is a 7-bit ASCII file. It is then processed.
For each line, a tweet object is created and a unique identifier (sequence number)
is assigned to each tweet. The tweet object also contains a link back to the owner
of the tweet so that printing out the tweet can be made more efficient. The tweet
identifier is also associated with the tweet owner.

Printing:

When printing all tweets to the console, for each user in the tweet system, a
list of all users that individual is following is generated. For that list, the
combined list of all tweets is generated and ordered by the sequence number.
The list is then printed and the routine moves to the next user in the list.

Assumptions:

- Each user in the tweet file must be present in the user file or a processing
  error will be thrown
- An > sign may be used in the tweet body
- A tweet username may contain a space
- A tweet may not be empty