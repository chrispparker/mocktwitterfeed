package util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import za.co.allangray.util.StringUtils;

/**
 * Created by chris on 2017/05/07.
 */
@RunWith(SpringJUnit4ClassRunner.class)

public class StringUtilsTest {

    @Test
    public void sevenBitStringShouldBeValidTest(){
        String stringToTest = "This string is valid";
        assert(StringUtils.validateStringIsSevenBitAscii(stringToTest)) ;
    }

    @Test
    public void eightBitStringShouldNotBeValidTest(){
        String stringToTest = "This string is valid ë";
        assert(!StringUtils.validateStringIsSevenBitAscii(stringToTest)) ;
    }
}
