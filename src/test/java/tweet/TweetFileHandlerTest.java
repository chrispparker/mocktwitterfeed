package tweet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import za.co.allangray.domain.Tweet;
import za.co.allangray.domain.User;
import za.co.allangray.feed.TweetFileHandler;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by chris on 2017/05/07.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@PropertySource("classpath:application.properties")
@SpringBootApplication
@ComponentScan(basePackages = {"za.co.allangray.*"})
public class TweetFileHandlerTest {

    @Autowired
    ApplicationContext ctx;

    @Test
    public void validTweetInputLineTest() throws Exception {
        String lineToTest = "Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.";
        SortedMap<String, User> userList = new TreeMap();
        userList.put("Ward", new User("Ward"));
        Tweet tweet = ctx.getBean(TweetFileHandler.class).processLine(lineToTest, 1, userList);
        assert(tweet.getOwner().getName().equals("Ward")) ;
    }

    @Test (expected = Exception.class)
    public void invalidTweetInputLineTest() throws Exception {
        String lineToTest = "Ward There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.";
        SortedMap<String, User> userList = new TreeMap();
        userList.put("Ward", new User("Ward"));
        ctx.getBean(TweetFileHandler.class).processLine(lineToTest, 1, userList);
    }

    @Test (expected = Exception.class)
    public void tweetTooLongInputLineTest() throws Exception {
        String lineToTest = "Ward> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque interdum rutrum sodales. Nullam mattis fermentum libero, non volutpat.!";
        SortedMap<String, User> userList = new TreeMap();
        userList.put("Ward", new User("Ward"));
        ctx.getBean(TweetFileHandler.class).processLine(lineToTest, 1, userList);
    }

    @Test (expected = Exception.class)
    public void userMissingForTweetInputLineTest() throws Exception {
        String lineToTest = "Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.";
        SortedMap<String, User> userList = new TreeMap();
        ctx.getBean(TweetFileHandler.class).processLine(lineToTest, 1, userList);
    }

    @Test (expected = Exception.class)
    public void emptyTweetInputLineTest() throws Exception {
        String lineToTest = "Ward> ";
        SortedMap<String, User> userList = new TreeMap();
        ctx.getBean(TweetFileHandler.class).processLine(lineToTest, 1, userList);
    }
}
