package user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import za.co.allangray.domain.User;
import za.co.allangray.feed.UserFileHandler;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by chris on 2017/05/07.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@PropertySource("classpath:application.properties")
@SpringBootApplication
@ComponentScan(basePackages = {"za.co.allangray.*"})
public class UserFileHandlerTest {

    @Autowired
    ApplicationContext ctx;

    @Test
    public void validUserInputLineTest() throws Exception {
        String lineToTest = "Ward follows Alan";
        SortedMap<String, User> userList = new TreeMap();
        SortedMap<String, User> stringUserSortedMap = ctx.getBean(UserFileHandler.class).processLine(lineToTest, userList);

        assert(stringUserSortedMap.get("Ward").getName().equals("Ward")) ;
        assert(stringUserSortedMap.get("Ward").getFollowing().get("Ward").getName().equals("Ward")) ;
    }

    @Test(expected = Exception.class)
    public void invalidUserInputLine1Test() throws Exception {
        String lineToTest = "Wardollows Alan";
        SortedMap<String, User> userList = new TreeMap();
        ctx.getBean(UserFileHandler.class).processLine(lineToTest, userList);
    }

    @Test(expected = Exception.class)
    public void invalidUserInputLine2Test() throws Exception {
        String lineToTest = "Ward follows";
        SortedMap<String, User> userList = new TreeMap();
        ctx.getBean(UserFileHandler.class).processLine(lineToTest, userList);
    }


    @Test
    public void multiFollowsUnionUserInputLineTest() throws Exception {
        String lineToTest1 = "Ward follows Alan";
        String lineToTest2 = "Ward follows Alan, Martin";
        SortedMap<String, User> userList = new TreeMap();
        userList = ctx.getBean(UserFileHandler.class).processLine(lineToTest1, userList);
        userList = ctx.getBean(UserFileHandler.class).processLine(lineToTest2, userList);

        assert(userList.get("Ward").getFollowing().get("Alan") != null);
        assert(userList.get("Ward").getFollowing().get("Martin") != null);
        assert(userList.get("Ward").getFollowing().get("Ward") != null);
    }
}
