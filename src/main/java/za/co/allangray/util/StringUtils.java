package za.co.allangray.util;

/**
 * Created by chris on 2017/05/07.
 */
public class StringUtils {
    /**
     * Determines whether a string is a valid 7-bit ascii string
     * @param inputString The string to be tested
     * @return True if the string is a valid 7-bit string, false if not
     */
    public static boolean validateStringIsSevenBitAscii(String inputString){
        for (char z : inputString.toCharArray()) {
            if (z >= 127) {
                return false;
            }
        }
        return true;
    }
}
