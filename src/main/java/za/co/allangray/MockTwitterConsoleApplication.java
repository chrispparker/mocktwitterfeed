package za.co.allangray;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import za.co.allangray.domain.User;
import za.co.allangray.feed.TweetFileHandler;
import za.co.allangray.feed.UserFileHandler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.SortedMap;

/**
 * Created by chris on 2017/05/06.
 */
@PropertySource("classpath:application.properties")
@SpringBootApplication
@ComponentScan(basePackages = {"za.co.allangray.*"})
public class MockTwitterConsoleApplication {

    private static final Logger LOGGER = Logger.getLogger(MockTwitterConsoleApplication.class);

    /**
     * The entry point into the application
     * @param args Two arguments from the command line, the first being the user file, the second being the tweet file
     */
    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(MockTwitterConsoleApplication.class, args);

        if (args.length < 2){
            LOGGER.error("Insufficient arguments provided. Options : <userFile> <tweetFile> ");
        }else {
            SortedMap<String, User> users = null;
            try {
                users = ctx.getBean(UserFileHandler.class).processFile(args[0]);
            } catch (IOException e) {
                LOGGER.error(String.format("The input file at path %s cannot be read", args[0]));
                e.printStackTrace();
            }

            try {
                TweetFileHandler tweetFileHandler = ctx.getBean(TweetFileHandler.class);
                tweetFileHandler.processFile(args[1], users);
                tweetFileHandler.printTweetsToConsole(users);
            } catch (IOException e) {
                LOGGER.error(String.format("The input file at path %s cannot be read", args[1]));
                e.printStackTrace();
            } catch (Exception e){

            }
        }
    }
}
