package za.co.allangray.feed;

import za.co.allangray.domain.User;

import java.io.IOException;
import java.util.SortedMap;

/**
 * Created by chris on 2017/05/06.
 */
public interface TweetHandler {

    /**
     * Processes the tweet file. Checks the file for validity as well as generating the structures required for printing
     * @param userFilePath The path to the file containing tweets
     * @param users A list of users on the tweet platform
     */
    void processFile(String userFilePath, SortedMap<String, User> users) throws Exception;

    /**
     * Prints the result of processing to the console for each user in the format:
     * @param users The list of users for which to print out tweets
     */
    void printTweetsToConsole(SortedMap<String, User> users);
}
