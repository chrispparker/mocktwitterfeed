package za.co.allangray.feed;

import za.co.allangray.domain.User;

import java.io.IOException;
import java.util.SortedMap;

/**
 * Created by chris on 2017/05/06.
 */
public interface UserHandler {

    /**
     * Checks the user file for validity as well as generating the structures required for printing
     * @param userFilePath The path to the input file containing users
     * @return returns a map of users keyed on username
     */
    SortedMap<String, User> processFile(String userFilePath) throws IOException;
}
