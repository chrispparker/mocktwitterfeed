package za.co.allangray.feed;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import za.co.allangray.domain.Tweet;
import za.co.allangray.domain.User;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by chris on 2017/05/06.
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TweetFileHandler extends AbstractFileHandler implements TweetHandler {

    private static final Logger LOGGER = Logger.getLogger(TweetFileHandler.class);

    private SortedMap<Long, Tweet> tweetMap = new TreeMap();

    /**
     * Process the tweet file and create a map containing all tweets linking them back to the user that made them
     * @param filePath The path to the file containing the tweets
     * @param users A map of all users on the tweet platform
     */
    public void processFile(String filePath, SortedMap<String, User> users) throws Exception {
        super.validateFile(filePath);
        long sequenceNumber = 1L;

        BufferedReader fileReader = new BufferedReader(new FileReader(filePath));
        String line;
        while ((line = fileReader.readLine()) != null) {
            Tweet tweet = processLine(line, sequenceNumber, users);
            tweetMap.put(sequenceNumber, tweet);
            users.get(tweet.getOwner().getName()).addTweet(sequenceNumber);
            sequenceNumber++;
        }
    }

    /**
     * Validates and processes a line in the tweet input file. As tweets are processed, a unique value
     * is assigned to each tweet so as to preserve the order in which the tweets were made. In the absence of a
     * timestamp in the input file, a long is used for sake of simplicity.
     * @param line The line to be processed
     * @param sequenceNumber The sequence number of the tweet
     * @param users A map of users on the tweet platform
     * @return The resulting tweet object after line processing
     */
    public Tweet processLine(String line, long sequenceNumber, SortedMap<String, User> users) throws Exception {
        String[] inputLine = line.split("> ", 2);

        if ((inputLine.length != 2 || inputLine[0].trim().length() == 0 || inputLine[1].trim().length() == 0)) {
            String error = "The tweet input file is malformed. Ensure the file is in the format [<user>> <tweet>]. ";
            LOGGER.error(error);
            throw new Exception(error);
        }

        if (inputLine[1].trim().length() > 140) {
            String error = String.format("A tweet may only be 140 characters long. Offending tweet by %s : %s", inputLine[0].trim(), inputLine[1].trim());
            LOGGER.error(error);
            throw new Exception(error);
        }

        String tweetOwner = inputLine[0].trim();
        String tweetContents = inputLine[1].trim();

        if (users.get(tweetOwner) == null) {
            String error = String.format("The user %s for tweet %s was not found in the user file", tweetOwner, tweetContents);
            LOGGER.error(error);
            throw new Exception(error);
        }
        return new Tweet(sequenceNumber, tweetContents, users.get(tweetOwner));
    }

    /**
     * Prints the tweets a user has made as well as those of his/her followers to the console in the format:
     * <tab>@user:<space>message.
     * Each user is listed alphabetically with their respective tweets listed directly below
     * @param users The map of users on the tweet platform
     */
    @Override
    public void printTweetsToConsole(SortedMap<String, User> users) {
        for (Map.Entry<String, User> user : users.entrySet()){
            System.out.println(user.getValue().getName());

            List<Long> tweetSet = new ArrayList();
            for (Map.Entry<String, User> followingUser : user.getValue().getFollowing().entrySet()){
                tweetSet.addAll(followingUser.getValue().getTweetIds());
            }
            Collections.sort(tweetSet);

            for (long tweetId : tweetSet){
                Tweet tweet = tweetMap.get(tweetId);
                System.out.println(String.format("\t@%s: %s", tweet.getOwner().getName(), tweet.getContents()));
            }
        }
    }
}
