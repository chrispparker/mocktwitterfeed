package za.co.allangray.feed;

import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static za.co.allangray.util.StringUtils.validateStringIsSevenBitAscii;

/**
 * Created by chris on 2017/05/06.
 */
public class AbstractFileHandler {

    private static final Logger LOGGER = Logger.getLogger(AbstractFileHandler.class);

    public void validateFile(String filePath) throws IOException {
        validateSevenBitAsciiFile(filePath);
    }

    /**
     * Validates that the file path provided is a valid file on disk as well as whether the file contains
     * only 7-bit ASCII characters
     * @param filePath The path to the file to be validated
     */
    private void validateSevenBitAsciiFile(String filePath) throws IOException {
        Files.lines(Paths.get(filePath)).forEach((String i) -> {
                if (!validateStringIsSevenBitAscii(i)){
                    LOGGER.error(String.format("The file %s is not an Ascii 7-bit file and contains unsupported characters", filePath));
                }
        });
    }
}
