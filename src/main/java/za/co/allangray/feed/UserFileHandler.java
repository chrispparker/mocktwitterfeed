package za.co.allangray.feed;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import za.co.allangray.domain.User;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by chris on 2017/05/06.
 */
@Component
public class UserFileHandler extends AbstractFileHandler implements UserHandler {

    private static final Logger LOGGER = Logger.getLogger(UserFileHandler.class);

    @Override
    public SortedMap<String, User> processFile(String filePath) throws IOException {
        super.validateFile(filePath);
        SortedMap<String, User> userList = new TreeMap();
        try {
            BufferedReader fileReader = new BufferedReader(new FileReader(filePath));
            String line;
            while ((line = fileReader.readLine()) != null) {
                try {
                    userList = processLine(line, userList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userList;
    }

    /**
     * Validates and processes a line in the user input file
     * @param line The input line to parse
     * @param userList The resultant updated user list after processing the line
     */
    public SortedMap<String, User> processLine(String line, SortedMap<String, User> userList) throws Exception {
        String[] inputLine = line.split("follows");

        if ((inputLine.length != 2 || inputLine[0].trim().length() == 0 && inputLine[1].trim().length() == 0)) {
            String error = "The user/follower input file is malformed. Ensure the file is in the format [<user> follows <user>,<user>,<user>]";
            LOGGER.error(error);
            throw new Exception(error);
        }

        String primaryUser = inputLine[0].trim();
        String followingUsers = inputLine[1].trim();

        //Add new primary users to the list if they do not exist
        if (userList.get(primaryUser) == null){
            createNewUser(primaryUser, userList);
        }

        //Add followers
        for (String userToFollow : followingUsers.split(",")){
            if (userList.get(userToFollow.trim()) == null){
                createNewUser(userToFollow, userList);
            }
            userList.get(primaryUser).addUserToFollow(userList.get(userToFollow.trim())); //Ensure the user follows himself
        }
        return userList;
    }

    /**
     * Creates a new primary tweet platform user
     * The user to be added can also be a tweeting user that we have not seen in the user file before
     * @param username The username of the tweet platform user
     */
    private void createNewUser(String username, SortedMap<String, User> userList){
        User user = new User(username.trim());
        userList.put(user.getName(), user);
        userList.get(user.getName()).addUserToFollow(user);
    }
}
