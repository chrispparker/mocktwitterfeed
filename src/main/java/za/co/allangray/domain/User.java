package za.co.allangray.domain;

import java.util.*;

/**
 * Created by chris on 2017/05/06.
 */
public class User {

    private String name;
    private List<Long> tweetIds = new ArrayList();
    private Map<String, User> following = new HashMap();

    public User (String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Long> getTweetIds() {
        return tweetIds;
    }

    public void addTweet(long tweetId) {
        this.tweetIds.add(tweetId);
    }

    public  Map<String, User> getFollowing() {
        return following;
    }

    public void addUserToFollow(User user) {
        this.following.put(user.getName(), user);
    }
}
