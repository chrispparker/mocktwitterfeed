package za.co.allangray.domain;

/**
 * Created by chris on 2017/05/06.
 */
public class Tweet {

    private long sequenceNumber;
    private String contents;
    private User owner;

    public User getOwner() {
        return owner;
    }

    public Tweet (long sequenceNumber, String contents, User owner){
        this.sequenceNumber = sequenceNumber;
        this.contents = contents;
        this.owner = owner;
    }

    public String getContents() {
        return contents;
    }
}
